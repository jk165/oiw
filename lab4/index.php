﻿<!DOCTYPE html>
<html >
	<head>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<script type="text/javascript" src="scripts/jquery-3.3.1.js"></script>
		<script src = "scripts/refreshing.js"></script>
	</head>
	<body class="container">
			<span style="font-size:30px;cursor:pointer" onclick="opening()">&#9776;</span>

		<header>
		<center>
			<a href = "index.php"><img src = "images/logo.jpg" id="logo"></a>
			</center>
			<nav>
			<div id="menu" class ="menu">
			<a href = "javascript:void(0)" class = "close" onclick="closing()">&times;</a>
			<a href= "loguj.php">Loguj</a>
			<a href= "rejestruj.php">Rejestruj</a>
			<a href="dodaj.php">Dodaj artykuł</a>
			</div>
			</nav>
		</header>
		<main id = "main" class ="container-fluid">	
			<div id="sql">
			<center>
				<?php
					include('scripts/sql.php');
				?>
			</center>
			</div>
		</main>
		<footer>
		<h5>Jakub Kowalski</h5>
		</footer>
		<script src="scripts/slider.js"></script>
	</body>
</html>