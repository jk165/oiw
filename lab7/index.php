﻿<?php 
 include("scripts/session.php");
?>
<!DOCTYPE html>
<html >
	<head>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<script type="text/javascript" src="scripts/jquery-3.3.1.js"></script>
		<script src = "scripts/refreshing.js"></script>
		<title>Blog</title>
		<link rel="Shortcut icon" href="images/miniatura.png" />
	</head>
	<body class="container">
			<span style="font-size:30px;cursor:pointer" onclick="opening()">&#9776;</span>

		<header>
		<center>
			<a href = "index.php" ><img src = "images/logo.jpg"  class="img-fluid"></a>
			</center>
			<nav>
			<div id="menu" class ="menu">
			<a href = "javascript:void(0)" class = "close" onclick="closing()">&times;</a>
			<a href= "loguj.php">Loguj</a>
			<a href= "rejestruj.php">Rejestruj</a>
			<a href="dodaj.php">Dodaj artykuł</a>
			</div>
			</nav>
			
		</header>
		<main id = "main" class ="container-fluid">	
		<hr>
		<div class='alert alert-secondary'>
					<?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
      	<h2>
          <?php 
          	echo "<div class='alert alert-success'>".$_SESSION['success']."</div>"; 
          	unset($_SESSION['success']);
          ?>
      	</h2>
      </div>
  	<?php endif ?>

    <!-- logged in user information -->
    <?php  if (isset($_SESSION['username'])) : ?>
    	<p>Witaj <strong><?php echo $_SESSION['username']; ?></strong></p>
    	<p> <a href="index.php?logout='1'" class="btn btn-danger">logout</a> </p>
    <?php endif ?>
	</div>
<hr>
<div class='alert alert-secondary'>
		<form action ="filters.php" method ="post" class="form-group">
			<label for="head" class="display-4">Nagłówek</label>
			<input name="head" type="text" placeholder="wyszykaj nagłówka" class="form-control">
			<label for="dating" class="display-4">Data dodania</label>
			<input name = "dating" type="date" class="form-control">
			<label for="autor" class="display-4">Autor artykułu</label>
			<input name="autor" type ="text" placeholder = "autor" class="form-control">
			<input type ="submit" value="Wyszukaj" class="btn btn-dark">
		</form>
		</div>
		<hr>	
	<div id="abc">
			<center>
				<?php
					include('scripts/show.php');
				?>
			</center>
			</div>
		</main>
		<footer>
		<h5>Jakub Kowalski</h5>
		</footer>
		<script src="scripts/slider.js"></script>
		
	</body>
</html>