<?php 
 include("scripts/session.php");
?>
<!DOCTYPE html>
<html >
	<head>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<script type="text/javascript" src="scripts/jquery-3.3.1.js"></script>
		<title>Edycja posta</title>
		<link rel="Shortcut icon" href="images/miniatura.png" />
	</head>
	<body class="container">
			<span style="font-size:30px;cursor:pointer" onclick="opening()">&#9776;</span>

		<header>
		<center>
			<a href = "index.php" ><img src = "images/logo.jpg"  class="img-fluid"></a>
			</center>
			<nav>
			<div id="menu" class ="menu">
			<a href = "javascript:void(0)" class = "close" onclick="closing()">&times;</a>
			<a href= "loguj.php">Loguj</a>
			<a href= "rejestruj.php">Rejestruj</a>
			<a href="dodaj.php">Dodaj artykuł</a>
			</div>
			</nav>
			
		</header>
		<main id = "main" class ="container-fluid">	
			<div id="abc">
			<center>
			<hr>
		<div class='alert alert-secondary'>
					<?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
      	<h2>
          <?php 
          	echo "<div class='alert alert-success'>".$_SESSION['success']."</div>"; 
          	unset($_SESSION['success']);
          ?>
      	</h2>
      </div>
  	<?php endif ?>

    <!-- logged in user information -->
    <?php  if (isset($_SESSION['username'])) : ?>
    	<p>Witaj <strong><?php echo $_SESSION['username']; ?></strong></p>
    	<p> <a href="index.php?logout='1'" class="btn btn-danger">logout</a> </p>
    <?php endif ?>
	</div>
		<hr>
				<?php
						include("scripts/view.php");
						
					
				?>
		<div class='alert alert-secondary'>
			<form method = "post" action="update.php">
				<h1 class="display-3">Edytuj artykuł</h1>
				<input type="hidden" name="login" value="<?php echo $_SESSION['username']; ?>">
				<input type="hidden" name="id" value="<?php echo $id; ?>">
				<label for="head">Nagłówek:</label>
				<textarea name = "head" rows ="10" cols="70" class="form-control" ><?php echo $row['head'];?></textarea><br>
				<label for="main">Artykuł:</label>
				<textarea name="main" rows= "10" cols = "70" class="form-control"><?php echo $row['main'];?></textarea><br>
				<input type="submit" value="Zatwierdź" class="btn btn-dark"> <a href ="index.php" class="btn btn-dark">Powrót</a>
			</form>
		</div>
			</center>
			</div>
		</main>
		<footer>
		<h5>Jakub Kowalski</h5>
		</footer>
		<script src="scripts/slider.js"></script>
		
	</body>
</html>