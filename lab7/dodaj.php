<?php 
 include("scripts/session.php");
?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
		<title>Dodawanie posta</title>
		<link rel="Shortcut icon" href="images/miniatura.png" />
	</head>
	<body class="container">
			<span style="font-size:30px;cursor:pointer" onclick="opening()">&#9776;</span>

		<header>
		<center>
			<a href = "index.php"><img src = "images/logo.jpg" class="img-fluid"></a>
		</center>
			<nav>
			<div id="menu" class ="menu">
			<a href = "javascript:void(0)" class = "close" onclick="closing()">&times;</a>
			<a href= "loguj.php">Loguj</a>
			<a href= "rejestruj.php">Rejestruj</a>
			<a href="dodaj.php">Dodaj artykuł</a>
			</div>
			</nav>
			
		</header>
		<main id = "main" class ="container-fluid">
		<hr>
		<div class='alert alert-secondary'>
					<?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" >
      	<h2>
          <?php 
          	echo "<div class='alert alert-success'>".$_SESSION['success']."</div>"; 
          	unset($_SESSION['success']);
          ?>
      	</h2>
      </div>
  	<?php endif ?>

    <!-- logged in user information -->
    <?php  if (isset($_SESSION['username'])) : ?>
    	<p>Witaj <strong><?php echo $_SESSION['username']; ?></strong></p>
    	<p> <a href="index.php?logout='1'" class="btn btn-danger">logout</a> </p>
    <?php endif ?>
	</div>
		<hr>
			<center>
				<div class='alert alert-secondary'>
				<form action="insert.php" method = "post">
				<h1 class="display-3">Dodaj artykuł</h1>
				<hr>
					<input type="hidden" name="login" value="<?php echo $_SESSION['username']; ?>">
					<label for="head">Nagłówek:</label>
					<textarea name = "head" rows ="10" cols="70" class="form-control"></textarea><br>
					<label for="main">Artykuł:</label>
					<textarea name="main" rows= "10" cols = "70" class="form-control"></textarea><br>
					<input type="submit" value="dodaj" class="btn btn-dark">
				</form>
				</div>
			</center>
			
		</main>
		<footer>
		<h5>Jakub Kowalski</h5>
		</footer>
		<script src="scripts/slider.js"></script>
	</body>
</html>