<?php include("scripts/server.php") ?>
<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<script type="text/javascript" src="scripts/jquery-3.3.1.js"></script>
		<title>Rejestracja</title>
		<link rel="Shortcut icon" href="images/miniatura.png" />
	</head>
	<body class="container">
			<span style="font-size:30px;cursor:pointer" onclick="opening()">&#9776;</span>

		<header>
		<center>
			<a href = "index.php"><img src = "images/logo.jpg" class="img-fluid"></a>
			</center>
			<nav>
			<div id="menu" class ="menu">
			<a href = "javascript:void(0)" class = "close" onclick="closing()">&times;</a>
			<a href= "loguj.php">Loguj</a>
			<a href= "rejestruj.php">Rejestruj</a>
			<a href="dodaj.php">Dodaj artykuł</a>
			</div>
			</nav>
			
		</header>
		<hr>
		<main id = "main" class ="container-fluid">		
			<center>
			<div class='alert alert-secondary'>
				<form action = "rejestruj.php" method="post" class="form-group">
				<?php include("scripts/errors.php"); ?>
				<label for="login">Login:</label>
				<input type="text" name = "login" class="form-control" placeholder="Podaj login:"><br>
				<label for="pass1">Hasło:</label>
				<input type = "password" name = "pass1" class="form-control" placeholder="Podaj Hasło:"><br>
				<label for="pass2">Potwierdź hasło:</label>
				<input type = "password" name = "pass2" class="form-control" placeholder="Podaj Hasło:"><br>
				<button type="submit" class="btn btn-dark" name="reg_user">Rejestruj</button>
				</div>
			</center>
		</main>
		<footer>
		<h5>Jakub Kowalski</h5>
		</footer>
		<script src="scripts/slider.js"></script>
		<script type= ></script>
	</body>
</html>