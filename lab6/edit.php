<!DOCTYPE html>
<html >
	<head>
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<script type="text/javascript" src="scripts/jquery-3.3.1.js"></script>
		
	</head>
	<body class="container">
			<span style="font-size:30px;cursor:pointer" onclick="opening()">&#9776;</span>

		<header>
		<center>
			<a href = "index.php" ><img src = "images/logo.jpg"  class="img-fluid"></a>
			</center>
			<nav>
			<div id="menu" class ="menu">
			<a href = "javascript:void(0)" class = "close" onclick="closing()">&times;</a>
			<a href= "loguj.php">Loguj</a>
			<a href= "rejestruj.php">Rejestruj</a>
			<a href="dodaj.php">Dodaj artykuł</a>
			</div>
			</nav>
			
		</header>
		<main id = "main" class ="container-fluid">	
			<div id="abc">
			<center>
				<?php
						include("scripts/view.php");
						
					
				?>
								<form method = "post" action="update.php">
				<h1 class="display-3">Edytuj artykuł</h1>
				<input type="hidden" name="id" value="<?php echo $id; ?>">
					<label for="head">Nagłówek:</label>
					<textarea name = "head" rows ="10" cols="70" class="form-control" ><?php echo $row['head'];?></textarea><br>
					<label for="main">Artykuł:</label>
					<textarea name="main" rows= "10" cols = "70" class="form-control"><?php echo $row['main'];?></textarea><br>
					<input type="submit" value="Zatwierdź" class="btn btn-dark"> <a href ="index.php" class="btn btn-dark">Powrót</a>
				</form>
				
			</center>
			</div>
		</main>
		<footer>
		<h5 class="display-5">Jakub Kowalski</h5>
		</footer>
		<script src="scripts/slider.js"></script>
		
	</body>
</html>