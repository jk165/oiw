<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	</head>
	<body class="container">
			<span style="font-size:30px;cursor:pointer" onclick="opening()">&#9776;</span>

		<header>
		<center>
			<a href = "index.php"><img src = "images/logo.jpg" class="img-fluid"></a>
		</center>
			<nav>
			<div id="menu" class ="menu">
			<a href = "javascript:void(0)" class = "close" onclick="closing()">&times;</a>
			<a href= "loguj.php">Loguj</a>
			<a href= "rejestruj.php">Rejestruj</a>
			<a href="dodaj.php">Dodaj artykuł</a>
			</div>
			</nav>
			
		</header>
		<main id = "main" class ="container-fluid">
		
			<center>
				
				<form action="insert.php" method = "post">
				<h1 class="display-3">Dodaj artykuł</h1>
					<label for="head">Nagłówek:</label>
					<textarea name = "head" rows ="10" cols="70" class="form-control"></textarea><br>
					<label for="main">Artykuł:</label>
					<textarea name="main" rows= "10" cols = "70" class="form-control"></textarea><br>
					<input type="submit" value="dodaj" class="btn btn-dark">
				</form>
			</center>
			
		</main>
		<footer>
		<h5 class="display-5">Jakub Kowalski</h5>
		</footer>
		<script src="scripts/slider.js"></script>
	</body>
</html>