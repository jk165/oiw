<?php include('scripts/server.php') ?>
<!DOCTYPE html>
<html>
	<head>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="styles/style.css">
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src = "scripts/refreshing.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Blog</title>
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
	</head>
	<body class="container">


		<header>
		<center>
            <nav class="navbar navbar-expand-sm bg-dark">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href= "index.php"><i class="fas fa-home"></i>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href= "loguj.php">Loguj</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href= "rejestruj.php">Rejestruj</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="dodaj.php">Dodaj artykuł</a>
                    </li>
                </ul>
            </nav>
			
		</header>
		<hr>
		<main id = "main" class ="container-fluid">		
			<center >
			<div class='alert alert-secondary'>
				<form action = "loguj.php" method="post" class="form-group">
				<?php include('scripts/errors.php'); ?>
				<label for="login">Login:</label>
				<input type="text" name = "login" class="form-control" placeholder="Podaj login:"><br>
				<label for="pass">Hasło:</label>
				<input type = "password" name = "pass" class="form-control" placeholder="Podaj Hasło:"><br>
				<button type="submit" class="btn btn-dark" name="login_user">Loguj</button>
				</form>
				</div>
			</center>
		</main>
        <footer>
            <h5>Jakub Kowalski</h5>
            <a href="http://ujeb.se/YFyM" class="fa fa-facebook"></a>
            <a href="http://ujeb.se/gtvqa" class="fa fa-linkedin"></a>
            <a href="http://ujeb.se/Snduc" class="fa fa-instagram"></a>
            <script src="scripts/slider.js"></script>
            <script src="scripts/angular1.js"></script>
        </footer>
	</body>
</html>