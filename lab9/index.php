﻿<?php 
 include("scripts/session.php");
?>
<!DOCTYPE html>
<html >
	<head>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src = "scripts/refreshing.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <title>Blog</title>
        <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    </head>
	<body class="container">

<div>

		<main id = "main" class ="container-fluid">
            <header>

                <nav class="navbar navbar-expand-sm bg-dark ">
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link" href= "index.php"><i class="fas fa-home"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href= "loguj.php">Loguj</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href= "rejestruj.php">Rejestruj</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="dodaj.php">Dodaj artykuł</a>
                        </li>
                        <li class="nav-item">
                            <?php  if (isset($_SESSION['username'])) : ?>
                                <a class="nav-link" href="index.php?logout='1'">Wyloguj</a>
                            <?php endif ?>
                        </li>


                    </ul>
                </nav>

            </header>
					<?php if (isset($_SESSION['success'])) : ?>
      <div class="error success" ng-app="myApp" ng-controller="myCtrl">

      	<h2>
          <?php 
          	echo "<div class='alert alert-success'>".$_SESSION['success']."</div>"; 
          	unset($_SESSION['success']);
          ?>
      	</h2>
      </div>
  	<?php endif ?>
<div class='alert alert-secondary'>
    <div  ng-app="myApp" ng-controller="myCtrl">
        <button ng-click="myFunc()" class="btn btn-info" >Filtry</button>
        <div ng-show="showMe" class="filtry">
		<form action ="filters.php" method ="post" class="form-group">
			<label for="head" class="display-4">Nagłówek</label>
			<input name="head" type="text" placeholder="wyszykaj nagłówka" class="form-control">
			<label for="dating" class="display-4">Data dodania</label>
			<input name = "dating" type="date" class="form-control">
			<label for="autor" class="display-4">Autor artykułu</label>
			<input name="autor" type ="text" placeholder = "autor" class="form-control">
			<input type ="submit" value="Wyszukaj" class="btn btn-dark">
		</form>
        </div>
    </div>
		</div>

		<hr>	
	<div id="abc">
			<center>
				<?php
					include('scripts/show.php');
				?>
			</center>
			</div>
		</main>
		<footer>
        <div class="chip">
        <img src="images\face.png" alt="Ja" width="96" height="96">
        Jakub Kowalski
        </div>
            <a href="http://facebook.com" class="fa fa-facebook"></a>
            <a href="http://linkedin.com" class="fa fa-linkedin"></a>
            <a href="http://instagram.com" class="fa fa-instagram"></a>
            <script src="scripts/slider.js"></script>
            <script src="scripts/angular1.js"></script>
		</footer>
</div>
	</body>
</html>