


-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych information_schema

-- Zrzut struktury bazy danych projekt_html
CREATE DATABASE IF NOT EXISTS `projekt_html` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;
USE `projekt_html`;

-- Zrzut struktury tabela projekt_html.artykuly
CREATE TABLE IF NOT EXISTS `artykuly` (
  `id_a` int(11) NOT NULL AUTO_INCREMENT,
  `head` char(200) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `main` text CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `data_utworzenia` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `id_u` int(11) NOT NULL,
  `foto` text NOT NULL,
  `lajki` int(11) NOT NULL,
  PRIMARY KEY (`id_a`),
  KEY `id_u` (`id_u`),
  CONSTRAINT `artykuly_ibfk_1` FOREIGN KEY (`id_u`) REFERENCES `users` (`id_u`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

-- Zrzucanie danych dla tabeli projekt_html.artykuly: ~2 rows (około)
DELETE FROM `artykuly`;
/*!40000 ALTER TABLE `artykuly` DISABLE KEYS */;
INSERT INTO `artykuly` (`id_a`, `head`, `main`, `data_utworzenia`, `id_u`, `foto`, `lajki`) VALUES
	(14, 'Wstęp do bloga', 'Boli noga', NULL, 5, 'images/ojjakboli.jpg', 0),
	(15, 'Rozwinięcie', 'Wrzód na pięcie', NULL, 10, 'images/ojejuniu.jpg', 0);
/*!40000 ALTER TABLE `artykuly` ENABLE KEYS */;

-- Zrzut struktury tabela projekt_html.users
CREATE TABLE IF NOT EXISTS `users` (
  `id_u` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  `pass` varchar(100) CHARACTER SET utf8 COLLATE utf8_polish_ci NOT NULL,
  PRIMARY KEY (`id_u`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Zrzucanie danych dla tabeli projekt_html.users: ~10 rows (około)
DELETE FROM `users`;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id_u`, `login`, `pass`) VALUES
	(1, 'root', 'toor'),
	(2, 'admin', '81dc9bdb52d04dc20036dbd8313ed055'),
	(3, 'JK', '202cb962ac59075b964b07152d234b70'),
	(4, 'abc', '202cb962ac59075b964b07152d234b70'),
	(5, 'admin', '250cf8b51c773f3f8dc8b4be867a9a02'),
	(6, 'root', '81dc9bdb52d04dc20036dbd8313ed055'),
	(7, 'root', '99c5e07b4d5de9d18c350cdf64c5aa3d'),
	(8, 'doktor', '81dc9bdb52d04dc20036dbd8313ed055'),
	(9, 'qwe', '202cb962ac59075b964b07152d234b70'),
	(10, 'toot', '881cc4157ed641a365a86452f27ed745');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
